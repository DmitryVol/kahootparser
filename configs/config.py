import yaml
import logging.config


with open('configs/config.yaml', 'r') as config_file:
    config = yaml.safe_load(config_file)


# ------------------------------------------------------------------------------
#   LOGGING SETTINGS
# ------------------------------------------------------------------------------

logging.config.dictConfig(config['logging'])


def get_logger(name=None):
    return logging.getLogger(name)

