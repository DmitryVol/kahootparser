from functools import wraps

from configs.config import get_logger

logger = get_logger("decoratorslogger")


def log_exceptions(func):
    @wraps(func)
    def logged_func(*func_args, **func_kwargs):
        try:
            res = func(*func_args, **func_kwargs)
            return res
        except Exception as e:
            logger.error("Exception in {}!".format(func.__name__))
            logger.exception(e)
            raise e
    return logged_func
