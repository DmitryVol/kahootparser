import argparse

from server.resources import *


logger = get_logger('server')


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--host', '-H', default='127.0.0.1',
                        help='The server host')
    parser.add_argument('--port', '-P', default='8081',
                        help='The server port')
    parser.add_argument('--debug', '-DEBUG', action='store_true',
                        help='The server port')
    args = parser.parse_args()

    server.debug = args.debug
    server.run(host=args.host, port=args.port)
