# Quizlet parser


## Local version

```
python lingua_parser.py --url https://quizlet.com/36670171/%E3%81%92%E3%82%93%E3%81%8D_lesson2-flash-cards/ --output 'test.json'
```


## Remote version

Ensure to set SCARPERKEY with api key for proxy

Run server

```
python server.py --host 127.0.0.1 --port 8081
```

Input JSON:

```json
{
    "url": ["indian-culture/22c86ecc-ad1a-4deb-8f46-7a907480cbd9"]
}
```

Output JSON:

```json
{
  "cards": [
    {
      "imageUrl": "",
      "question": "What is the meaning of white colour used in the National Flag?",
      "choices": {
        "Sacrifice": false,
        "Truth and Purity of thoughts": true,
        "Prosperity of life": false,
        "None of the above": false
      }
    },
    {
      "imageUrl": "",
      "question": "Diwali is also known as the:",
      "choices": {
        "Festival of lights": true,
        "Festival of Winters": false,
        "Festival of Sweets": false,
        "Festival of colours": false
      }
    },
    {
      "imageUrl": "",
      "question": "Holi festival marks the end of winter and the beginning of?",
      "choices": {
        "Summer": false,
        "Spring": true,
        "Monsoon": false,
        "Autumn": false
      }
    },
    {
      "imageUrl": "",
      "question": "Kathak is classical dance of",
      "choices": {
        "NORTH INDIA": true,
        "SOUTH INDIA": false,
        "EAST INDIA": false,
        "WEST INDIA": false
      }
    },
    {
      "imageUrl": "",
      "question": "Which is the largest religious community of India?",
      "choices": {
        "Hindus": true,
        "Christians": false,
        "Muslims": false,
        "Sikhs": false
      }
    },
    {
      "imageUrl": "https://images-cdn.kahoot.it/0fd8bad0-9b22-49e5-93b4-86208a7c1fd2",
      "question": "Identify this dance form of India",
      "choices": {
        "KATHAK": false,
        "BHARATNATYAM": true,
        "MANIPURI": false,
        "LAVANI": false
      }
    },
    {
      "imageUrl": "",
      "question": "Which is the National game of India?",
      "choices": {
        "Cricket": false,
        "Hockey": true,
        "Base Ball": false,
        "Football": false
      }
    },
    {
      "imageUrl": "",
      "question": "Name an Indian who won Noble prize for Literature?",
      "choices": {
        "Mother Teresa": false,
        "H.G Khorana": false,
        "Rabindra Nath Tagore": true,
        "C.V Raman": false
      }
    },
    {
      "imageUrl": "",
      "question": "What is the elevation of highest mountain peak in India?",
      "choices": {
        "8846 m": false,
        "8586 m": true,
        "8888 m": false,
        "8467 m": false
      }
    },
    {
      "imageUrl": "",
      "question": "Who is the famous Indian Leader whose birthday is celebrated on 2 October?",
      "choices": {
        "Mahatma Gandhi": true,
        "Mother Teresa": false,
        "Vivekananda": false,
        "C.V Raman": false
      }
    }
  ]
}
```

