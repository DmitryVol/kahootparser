import argparse
from bs4 import BeautifulSoup
import re
import json
from configs.config import get_logger
from scraper_api import ScraperAPIClient
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
import time
from decorators import log_exceptions
from webdriver_manager.chrome import ChromeDriverManager

logger = get_logger("parser")
service_args = [
    '--proxy=217.156.252.118:8080',
    '--proxy-type=https',
]
scarper_key = "7751edaea308076057eb2c71153ba0a1"
if scarper_key is None:
    logger.error("Unable to retreive a scarper ptoxy API key")
    raise Exception("Unable to send a request without proxy. "
                    "Check SCARPERKEY in env varaibles.")


class Parser:
    def __init__(self):
        self.proxy_client = ScraperAPIClient(scarper_key)

    @log_exceptions
    def get_soup(self, url):
        html = self.proxy_client.get(url=url).text
        with open("raw.html", "w") as f:
            f.write(html)
        soup = BeautifulSoup(html, 'html.parser')
        logger.debug("html from {} extracted".format(url))
        return soup

    @staticmethod
    @log_exceptions
    def extract_kahoot(links=None):
        chrome_options = Options()
        chrome_options.add_argument("--headless")
        cards = []
        if links is None:
            links = ['kahoot-with-groot/9983b771-65bc-4175-af56-d647d3cabc8d',
                    # 'class-review-8th-grade/66906f83-8d96-4c7a-a05e-5f2e0860f25d',
                    # 'indian-culture/22c86ecc-ad1a-4deb-8f46-7a907480cbd9'
                    ]
        for link in links:
            driver = webdriver.Chrome(ChromeDriverManager().install(), chrome_options=chrome_options)
            # driver.set_window_size(1120, 550)
            driver.get('https://create.kahoot.it/details/' + link)
            time.sleep(5)

            driver.find_element_by_class_name('text-button.question-list__group-'
                                              'toggle').click()

            soup = BeautifulSoup(driver.page_source, 'html.parser')
            ques = soup.find_all(
                'div',
                {'class': 'question-media__text-inner-wrapper'})
            ques_images = soup.find_all(
                'div',
                {'class': 'question-media__image'})
            im_urls = []
            for i in ques_images:
                link = re.findall(r'url\("(https:[\s\S]{20,200})\?auto=', str(i))
                if not link:
                    im_urls.append('')
                else:
                    im_urls.append(link[0])

            # print([i.find('background-image') for i in ques_images])
            chs = soup.find_all(
                'ul',
                {'class': 'choices'})

            cor = 'This is a correct'
            wron = 'This is a wrong'

            for i, c in enumerate(chs):

                card = {}
                choices_dict = {}

                card['imageUrl'] = im_urls[i]
                card['question'] = ques[i].text
                splitted_choices = c.text.split(' answer')
                for s_c in splitted_choices:
                    if cor in s_c:
                        choices_dict.update({s_c.replace(cor, ''): True})
                    elif wron in s_c:
                        choices_dict.update({s_c.replace(wron, ''): False})
                card['choices'] = choices_dict
                cards.append(card)

        return {
            'cards': cards}

    def extract(self, links):
        result = self.extract_kahoot(links)
        with open('example.json', 'w') as f:
            json.dump(result, f)
        return result


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Lingua Parser')
    parser.add_argument('--url', type=str, help='Flashcards URL')
    parser.add_argument('--output', type=str,
                        help='JSON file with extracted flashcards')
    args = parser.parse_args()
    p = Parser()
    urls = [  # 'kahoot-with-groot/9983b771-65bc-4175-af56-d647d3cabc8d',
        # 'class-review-8th-grade/66906f83-8d96-4c7a-a05e-5f2e0860f25d',
        'indian-culture/22c86ecc-ad1a-4deb-8f46-7a907480cbd9'
    ]
    #cards = p.extract(urls)
