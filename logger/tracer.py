from functools import wraps
from uuid import uuid4


class Tracer(object):
    def __init__(self):
        self.traceid = self.generate_traceid()

    @staticmethod
    def generate_traceid():
        return str(uuid4()).replace('-', '')

    @staticmethod
    def trace(traceid=None):
        def traced_func(func):
            @wraps(func)
            def wrapper(self, *args, **kwargs):
                if traceid:
                    tracer.traceid = traceid
                else:
                    tracer.traceid = Tracer.generate_traceid()
                return func(self, *args, **kwargs)
            return wrapper
        return traced_func


tracer = Tracer()
