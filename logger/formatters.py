import logging


class ColorFormatter(logging.Formatter):

    COLOR_BLACK = 30
    COLOR_RED = 31
    COLOR_GREEN = 32
    COLOR_BROWN = 33
    COLOR_BLUE = 34
    COLOR_MAGENTA = 35
    COLOR_CYAN = 36
    COLOR_GRAY = 37
    COLOR_YELLOW = 93
    COLOR_WHITE = 97
    COLOR_RED_BACKGROUND = 41
    COLOR_DEFAULT = 0

    _level_colors = {
        logging.DEBUG: COLOR_GRAY,
        logging.INFO: COLOR_DEFAULT,
        logging.WARN: COLOR_YELLOW,
        logging.WARNING: COLOR_YELLOW,
        logging.ERROR: COLOR_RED,
        logging.CRITICAL: COLOR_RED_BACKGROUND,
    }

    def __init__(self, fmt='%(levelname)s: %(msg)s', datefmt=None, style='%'):
        logging.Formatter.__init__(self, fmt=fmt, datefmt=datefmt, style=style)

    @staticmethod
    def _color_str(s, color=COLOR_GRAY):
        return '\033[{}m{}\033[0m'.format(color, s)

    def get_color_log(self, record):
        return self._color_str(self._fmt, self._level_colors[record.levelno])

    def format(self, record):
        # Remember the original format
        format_orig = self._fmt
        self._style._fmt = self.get_color_log(record)
        # Call the original formatter to do the grunt work
        result = logging.Formatter.format(self, record)
        # Restore the original format
        self._fmt = format_orig
        return result
