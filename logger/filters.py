import logging


from logger.tracer import tracer


class TraceFilter(logging.Filter):
    def filter(self, record):
        record.traceid = tracer.traceid
        return True
