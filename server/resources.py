import time

from flask import Response
from flask_restx import Resource

from server.instance import server

from server.models import  card_parser_input_model
from configs.config import get_logger
from tasks import parse_kahoot

logger = get_logger('server')

app, api = server.app, server.api
ns = api.namespace('', description='General operations')

last_extraction_request_time = None


@ns.route('/healthcheck')
class HealthCheck(Resource):
    @ns.doc('healthcheck')
    def get(self):
        return Response(status=200)


@ns.route('/extract_kahoot_cards')
class Document(Resource):
    @ns.doc('create document model')
    @api.expect(card_parser_input_model)
    def post(self):
        """Creates document model."""
        global last_extraction_request_time
        urls = api.payload["urls"]
        logger.info('Parse cards by url: {}'.format(urls))
        card_data = parse_kahoot(urls, last_extraction_request_time)
        last_extraction_request_time = time.time()

        return card_data

