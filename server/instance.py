from flask import Flask
from flask_restx import Api
from waitress import serve
from werkzeug.middleware.proxy_fix import ProxyFix


class Server(object):

    def __init__(self):
        self.app = Flask(__name__)
        self.app.wsgi_app = ProxyFix(self.app.wsgi_app)

        self.api = Api(self.app,
                       version="0.0.1",
                       title='Lingua card parser',
                       description='Lingua card parser',
                       validate=True)

        self.debug = False

    def run(self, **kwargs):
        if self.debug:
            self.app.run(host=kwargs.get('host'),
                         port=kwargs.get('port'),
                         debug=True)
        else:
            serve(self.app, **kwargs)


server = Server()
