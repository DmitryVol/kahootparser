import json
from flask_restx import fields

from server.instance import server

with open('server/examples/response.json', 'rt') as f:
    card_output_example = json.load(f)


answer_model = server.api.model('answer', {
    'firstChoice': fields.Boolean(),
    'secondChoice': fields.Boolean()
})



cards_model = server.api.model("cards", {
    "imageUrl": fields.String(),
    "question": fields.String(),
    "answer": fields.Nested(answer_model),
})


card_parser_output_model = server.api.model('parsed card model', {
    "cards": fields.List(fields.Nested(cards_model, example=card_output_example["cards"])),
})

card_parser_input_model = server.api.model("parse card input", {
    "urls": fields.List(fields.String())
})
