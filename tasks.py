import time

from kahoot_parser import Parser

p = Parser()


def parse_kahoot(urls, last_extraction_request_time):
    if last_extraction_request_time is not None:
        now = time.time()
        if now - last_extraction_request_time < 1.0:
            time.sleep(max(0.5, 1 - (now - last_extraction_request_time)))
    parsed_card = p.extract(urls)
    return parsed_card
